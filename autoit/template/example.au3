#include <Date.au3>
#include <Math.au3>
#include <MsgBoxConstants.au3>

If $CmdLine[0] > 1 Then
    $username = $CmdLine[1]
    $password = $CmdLine[2]
 Else
	SplashTextOn("Fortnite updater", "Ingen parametre...", -1, -1, -1, -1, $DLG_TEXTLEFT, "", 16)
	Sleep(10000)
  Exit
 EndIf

Example()

Func Example()
; blokere for mus og keyboard
BlockInput($BI_DISABLE)
; starter epic launcher og sætte vindue i hjørne og i en str vi kender
Run("C:\Program Files (x86)\Epic Games\Launcher\Engine\Binaries\Win64\EpicGamesLauncher.exe")
Local $hWnd = WinWait("[CLASS:UnrealWindow]", "", 10)
Local $aPos = WinGetPos($hWnd)
WinMove($hWnd, "", 0, 0, 1100, 600)
Sleep(60000)
MouseClick($MOUSE_CLICK_LEFT, 667, 264, 1)
; sender 30 del tast og 30 backspace på den måde stå der ikke noget når vi skriver
Send("{DEL 30}")
Send("{BACKSPACE 30}")
send($username)
MouseClick($MOUSE_CLICK_LEFT, 664, 351, 1)
Send("{DEL 30}")
Send("{BACKSPACE 30}")
send($password)
send ("{enter}")
sleep(2000)
; her rykker vi mus op og ser om der kommer en hvid banner der siger vi ikke kan logge ind. Hvis der er en hvis farve starter den forfra efter 10 sek
MouseMove ( 958, 199 )
$Color_W = 'FFFFFF'
  If Hex(PixelGetColor(958,199), 6) = $Color_W Then Call ("Restart")
sleep(90000)
; hvis ikke hvid farve venter vi 10 sek og rykker mus ned og trykker på download og bagefer rykker mus op hvor der stå hvor meget den skal downloade
MouseMove ( 108, 467 )
MouseClick($MOUSE_CLICK_LEFT, 108, 467, 1)
sleep(1000)
MouseMove ( 416, 207 )
sleep(90000)
; her venter vi på en grå farve der er normalt blåt her nå den downloader men når der ikke er update bliver der gråt også går den vidre
$Color_Green = '202020'
While 1
  If Hex(PixelGetColor(416,207), 6) = $Color_Green Then ExitLoop
  Wend
  ; her lukker den epic og tænder for mus og tastatur igen
 ProcessClose("EpicGamesLauncher.exe")
BlockInput($BI_ENABLE)
Exit
EndFunc

Func Restart ()
   ProcessClose("EpicGamesLauncher.exe")
   SplashTextOn("Fortnite updater", "Der var fejl prøver om 10 sek igen", -1, -1, -1, -1, $DLG_TEXTLEFT, "", 16)
Sleep(10000)
SplashOff()
   Call ("Example")
   EndFunc