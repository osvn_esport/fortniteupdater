$task_name = 'docker_lancache'
$user = 'System'
$base_path = 'C:\fortniteupdater'

# Create the task as normal
$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-NoProfile -ExecutionPolicy Bypass -File "$base_path\lancache\lancache.ps1"'
Register-ScheduledTask -Action $action -TaskName $task_name -Description "Making sure lancache is running, if lancache is not running - check that docker-desktop is running." -User $user

# Now add a special trigger to it with COM API.
# Get the service and task
$ts = New-Object -ComObject Schedule.Service
$ts.Connect()
$task = $ts.GetFolder("\").GetTask($task_name).Definition

# Create the trigger
$TRIGGER_TYPE_STARTUP=8
$startTrigger=$task.Triggers.Create($TRIGGER_TYPE_STARTUP)
$startTrigger.Enabled=$true
$startTrigger.Repetition.Interval="PT1M" # one minutes
$startTrigger.Repetition.StopAtDurationEnd=$false # on to infinity
$startTrigger.Id="StartupTrigger"

# Re-save the task in place.
$TASK_CREATE_OR_UPDATE=6
$ts.GetFolder("\").RegisterTaskDefinition($task_name, $task, $TASK_CREATE_OR_UPDATE, $null, $null, $null)