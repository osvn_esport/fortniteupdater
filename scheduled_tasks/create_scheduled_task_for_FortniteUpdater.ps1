$task_name = 'FortniteUpdater'
$user = 'System'
$base_path = 'C:\fortniteupdater'

$days_of_week = @("Monday", "Tuesday", "Wednesday", "Thursday", "Friday")
$time_of_day = (Get-Date -hour 14 -minute 00)
$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument "-ExecutionPolicy Bypass $($base_path)\FortniteUpdater.ps1 *> $($base_path)\runtime\log\sche_task_FortniteUpdater.log"
$trigger = New-ScheduledTaskTrigger -Weekly -DaysOfWeek $days_of_week -At $time_of_day
$task = Register-ScheduledTask -TaskName "FortniteUpdater" -Action $action -Trigger $trigger -RunLevel Highest -User $user