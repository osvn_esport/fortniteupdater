﻿[CmdletBinding()]
param (
    [Parameter()]
    [string]$base_path="c:\fortniteupdater",
    [Parameter()]
    [string]$ip_subnet="192.168.1.255"

    <#
    .DESCRIPTION
    
    FortniteUpdater.ps1

    .PARAMETER base_path
    Subnet scope for wake-on-lan magic packets, to be send within.

    .PARAMETER ip_subnet
    Subnet scope for wake-on-lan magic packets, to be send within.

    .EXAMPLE

    FortniteUpdater.ps1 -ipsubnet 192.168.1.255
    
    #>
)

# Declaring variables for later use in this script.
$fornite_runtime_path = "$base_path\runtime"
$client_lists_path = "$fornite_runtime_path\lists"
$log_folder_path = "$fornite_runtime_path\log"
$log_file_name = "GlobalRun.log"
$log_file_path = "$log_folder_path\$log_file_name"
$exe_path = "$base_path\autoit\executable"
$master_share_name = 'FortniteUpdaterLog'
$remote_log_path = "\\$env:COMPUTERNAME\$master_share_name\$log_file_name"

Write-Output ("{0} - ------------------------------- NEW RUN -------------------------------" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force

Set-Location -Path $fornite_runtime_path

if (!(Test-Path -Path ".\Invoke-parallel.ps1")) {
    Write-Output ("{0} - ERROR: There was a problem importing $fornite_runtime_path\Invoke-parallel.ps1, file was not found!" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
    exit 1
}

Import-Module ".\Invoke-parallel.ps1" -Force -ErrorAction Stop

if (!(Test-Path -Path ".\Send-WOL.ps1")) {
    Write-Output ("{0} - ERROR: There was a problem importing $fornite_runtime_path\Send-WOL.ps1, file was not found!" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
    exit 1
}

Import-Module ".\Send-WOL.ps1" -Force -ErrorAction Stop

# create share, if it does not exist
if (!(Get-SmbShare -Name $master_share_name)) {
    Write-Output ("{0} - Share does not exist for $log_folder_path, creating it..." -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
    try {
        New-SmbShare -Name $master_share_name -Path $log_folder_path -ChangeAccess "ALLE" -ErrorAction SilentlyContinue
        New-SmbShare -Name $master_share_name -Path $log_folder_path -ChangeAccess "Everyone" -ErrorAction SilentlyContinue}
catch {
    Write-Output ("{0} - ERROR: There was a problem creating share for $log_folder_path" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
}
}

# Make sure guest / gæst account is enabled
try {
    Write-Output ("{0} - Making sure Local guest account is enabled, for clients to reach log-file." -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
    Get-LocalUser -Name "gæst" | Enable-LocalUser -ErrorAction SilentlyContinue
    Get-LocalUser -Name "guest" | Enable-LocalUser -ErrorAction SilentlyContinue
}
catch {
    Write-Output ("{0} - ERROR: There was a problem enabling the guest account..." -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -Force
}

$jsonFiles = Get-ChildItem -Path "$client_lists_path\*.json" -File

$jsonFiles | ForEach-Object {

    $machines = Get-Content -raw -Path $_ | ConvertFrom-Json

    Write-Output ("{0} - Attempting to wake machines... $($machines | % {$_.machine})" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -force

    $machines | % { Send-WOL -mac $_.mac -ip $ip_subnet }

    Write-Output ("{0} - Running EpicUpdater on machines... $($machines | % {$_.machine})" -f [datetime]::Now) | Out-File -Append -FilePath $log_file_path -force

    $machines | Invoke-Parallel -RunspaceTimeout 7200 -ScriptBlock { 

        write-host $using:remote_log_path
        function WaitForWSMAN{
            Param(
                $computername
            )
            $msg='Timed out after {1:N0} seconds connecting to {0}.  Trying again'
            $starttime=[datetime]::Now
            While(!(Test-WSMan -computername $computername -ea 0)){
                Write-Host ( $msg -f $computername, $([datetime]::Now -$starttime).TotalSeconds) -fore green
                if ($([datetime]::Now -$starttime).TotalSeconds -gt 1200){
                    Write-Output ("{0} - Wakeup failed for $computername... $computername was not updated. exitting."  -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
                    exit 1
                }
            }
            Write-Output ('{1} - Host {0} connected at {1}' -f $computername, [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
        }
        
        function EpicUpdate {
            param (
                $obj
            )

            Write-Host "Initiating Test-WSMan..."
            WaitForWSMAN -computername $obj.machine

            Write-Host "Creating credentials for $($obj.machine) with user $($obj.win_username) and password $($obj.win_password)"
            $secpasswd = ConvertTo-SecureString $obj.win_password -AsPlainText -Force
            $mycreds = New-Object System.Management.Automation.PSCredential ($obj.win_username, $secpasswd)
        
            Invoke-Command -ComputerName $($obj.machine) -Credential $mycreds -ScriptBlock {
                if (!(Test-Path -Path "c:\temp")){
                    mkdir "c:\temp"
                }
            }
            
            Write-Host "Initiating session for $($obj.machine) with user $($obj.win_username) and password $($obj.win_password)..."
            $Session = New-PSSession -ComputerName $obj.machine -Credential $mycreds -ErrorVariable A -Verbose
            if($A) {
                Write-Output ("{0} - $($obj.machine): Copy $using:exe_path\$($obj.filename) to c:\temp\$($obj.filename) failed, could not establish session with $A" -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
                exit 1
            }

            Write-Host "Initiating Copy-Item for $($obj.machine) with file $using:exe_path\$($obj.filename)..."
            Copy-Item "$using:exe_path\$($obj.filename)" -Destination "c:\temp" -ToSession $Session -Force -ErrorVariable B -Verbose
            if($B) {
                Write-Output ("{0} - $($obj.machine): Copy $using:exe_path\$($obj.filename) to c:\temp\$($obj.filename) failed, was unable to copy file with $B" -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
            }        

            Write-Host "Initiating query session id for $($obj.machine)..."
            $sessionID = Invoke-Command -ComputerName $obj.machine -Credential $mycreds -ScriptBlock { 
                powershell -command 'query session | select-string console | foreach { -split $_ } | select -index 2'
            }
            if(!($sessionID)) {
                Write-host ("{0} - $($obj.machine): Query session id failed, was unable to retrieve sessionID" -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
                Write-Output ("{0} - $($obj.machine): Query session id failed, was unable to retrieve sessionID" -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
                exit 1
            }

            Write-Host ("{0} - $($obj.machine): session id for current user found = $sessionID" -f [datetime]::Now)
            Write-Output ("{0} - $($obj.machine): session id for current user found = $sessionID" -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
        
            Write-Host "Initiating sleep-timer 10 seconds for $($obj.machine)..."
            Start-Sleep -s 10
        
            Write-Output ("{0} - $($obj.machine): $using:fornite_runtime_path\PsExec64.exe \\$($obj.machine) -u $($obj.win_username) -p $($obj.win_password) -i $sessionID c:\temp\$($obj.filename) $($obj.epic_username) $($obj.epic_password) -accepteula" -f [datetime]::Now )| Out-File -Append -FilePath $using:remote_log_path -force
            cmd /c "$using:fornite_runtime_path\PsExec64.exe \\$($obj.machine) -u $($obj.win_username) -p $($obj.win_password) -i $sessionID c:\temp\$($obj.filename) $($obj.epic_username) $($obj.epic_password) -accepteula"

            if($LASTEXITCODE -ne 0) {                
                Write-Output ("{0} - $($obj.machine): Script failed for $($obj.machine), last exit code: $LASTEXITCODE"  -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
            }

            Write-Output ("{0} - $($obj.machine): $using:fornite_runtime_path\PsExec64.exe \\$($obj.machine) -u $($obj.win_username) -p $($obj.win_password) shutdown /s"  -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
            cmd /c "$using:fornite_runtime_path\PsExec64.exe \\$($obj.machine) -u $($obj.win_username) -p $($obj.win_password) shutdown /s"
            Write-Output ("{0} - $($obj.machine): Script completed for $($obj.machine), c:\temp\$($obj.filename), sessionid $sessionID"  -f [datetime]::Now) | Out-File -Append -FilePath $using:remote_log_path -force
        }

        EpicUpdate($_)
    }
}