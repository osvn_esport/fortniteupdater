# this script is part of the "lancache"-scheduled task
## Description ##
# The reason why we do this, is that docker-desktop needs some time to initiate, and we have to make sure its fully initiated before we can begin to deploy containers.
# We check if the 'docker --version'-command returns something with the text 'Docker version' in it.
# If true, we run 'docker-compose -d up' in the directory with the docker-compose.yaml-file - this will fire up the lancache containers.

$lancache_base_path = "C:\fortniteupdater\lancache\"

Set-Location -Path $lancache_base_path

$ip = $(Get-NetIPAddress -AddressFamily IPv4 | ? { $_.InterfaceAlias -eq 'Ethernet' }).IPAddress

if(!($ip)){
    Write-Output ("{0} - ERROR: could not retrieve the ip of the server: $ip" -f [datetime]::Now) | Out-File -Append -FilePath "$($lancache_base_path)\lancache\logs\lancache_setup.log" -Force
}

(Get-Content -Path .\docker-compose.yaml) |
    ForEach-Object {$_ -Replace '<YOUR IP HERE>', $ip} |
        Set-Content -Path .\docker-compose.yaml

Do
{
    $dVersion = docker --version
    start-sleep 5
} While ($dVersion -contains 'Docker version')

docker-compose up -d